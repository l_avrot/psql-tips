    <div class="tip">The <code>\gx</code>
      metacommand will execute the result of the current query or the last query
      if the current query buffer but will force the expanded output mode.
      <pre><code class="hljs bash">laetitia=# select * from pg_settings where name = 'log_directory';
     name      | setting | unit |               category               |                  short_desc                   |                               extra_desc                                | context | vartype | source  | min_val | max_val | enumvals | boot_val | reset_val | sourcefile | sourceline | pending_restart 
---------------+---------+------+--------------------------------------+-----------------------------------------------+-------------------------------------------------------------------------+---------+---------+---------+---------+---------+----------+----------+-----------+------------+------------+-----------------
 log_directory | log     |      | Reporting and Logging / Where to Log | Sets the destination directory for log files. | Can be specified as relative to the data directory or as absolute path. | sighup  | string  | default |         |         |          | log      | log       |            |            | f
(1 row)

laetitia=# \gx
-[ RECORD 1 ]---+------------------------------------------------------------------------
name            | log_directory
setting         | log
unit            | 
category        | Reporting and Logging / Where to Log
short_desc      | Sets the destination directory for log files.
extra_desc      | Can be specified as relative to the data directory or as absolute path.
context         | sighup
vartype         | string
source          | default
min_val         | 
max_val         | 
enumvals        | 
boot_val        | log
reset_val       | log
sourcefile      | 
sourceline      | 
pending_restart | f</code></pre>This feature is available since Postgres 10.
	</div>
