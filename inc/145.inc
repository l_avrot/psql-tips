    <div class="tip">
	  <code>\l pattern</code> will  display for each database matching the
		pattern, their names, owners access privileges and encoding and
		collation details.
      <pre><code class="hljs bash">laetitia=# \l laetitia
                                          List of databases
   Name   |  Owner   | Encoding | Collate | Ctype | ICU Locale | Locale Provider | Access privileges 
----------+----------+----------+---------+-------+------------+-----------------+-------------------
 laetitia | laetitia | UTF8     | C       | UTF-8 |            | libc            | 
(1 row)</code></pre>This feature is available
since Postgres 7.1, but was updated with Postgres 8.0, Postgres 8.4, Postgres
9.3, Postgres 15.
	</div>
