    <div class="tip">
      Use <code>\password</code> without any username to change securely the current user
      password.
      This command prompts for the new password, encrypts it, and sends it to
      the server as an ALTER ROLE command. This makes sure that the new password
      does not appear in cleartext in the command history, the server log, or
      elsewhere. That's the secure way to change passwords in Postgres.
      <pre><code class="hljs bash">laetitia=# \password
Enter new password for user "laetitia": 
Enter it again:</code></pre>This feature is available
since Postgres 8.2.
	</div>
