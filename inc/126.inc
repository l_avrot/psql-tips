    <div class="tip">The <code>\g</code> metacommand will execute the last query in the
    query buffer.
      <pre><code class="hljs bash">laetitia=# select * from test;
 id | value 
----+-------
  1 | bla
  2 | bla
  3 | bla
  4 | bla
  5 | bla
  6 | bla
(6 rows)

laetitia=# \g
 id | value 
----+-------
  1 | bla
  2 | bla
  3 | bla
  4 | bla
  5 | bla
  6 | bla
(6 rows)</code></pre>This feature is available
since at least Postgres 7.1.
	</div>
