    <div class="tip">
	The <code>&#43;</code> modifier to the <code>\l pattern</code> metacommand will, on top of displaying
	database names, owners, encoding, collation and access privileges
	details for databases matching the pattern, display size, default
	tablespace and comments.
      <pre><code class="hljs bash">laetitia=# \l+ laetitia
                                                           List of databases
   Name   |  Owner   | Encoding | Collate | Ctype | ICU Locale | Locale Provider | Access privileges | Size  | Tablespace | Description 
----------+----------+----------+---------+-------+------------+-----------------+-------------------+-------+------------+-------------
 laetitia | laetitia | UTF8     | C       | UTF-8 |            | libc            |                   | 10 MB | pg_default | 
(1 row)</code></pre>This feature is available
since Postgres 8.0.
	</div>
