    <div class="tip">You can use the <code>\g filename</code> metacommand to execute a query
      and store the result in the file named filename. Unless the complete path
      is given, the file will be stored in the current directory (that you can
	display with <code>\! pwd</code>
      <pre><code class="hljs bash">laetitia=# select * from test;
 id | value 
----+-------
  1 | bla
  2 | bla
  3 | bla
  4 | bla
  5 | bla
  6 | bla
(6 rows)

laetitia=# \g output.log
laetitia=# \! cat output.log
 id | value 
----+-------
  1 | bla
  2 | bla
  3 | bla
  4 | bla
  5 | bla
  6 | bla
(6 rows)

laetitia=# \! pwd
/Users/laetitia/tech/laetitia/psql-tips/tools</code></pre>This feature is available since at least Postgres 7.1.
	</div>
