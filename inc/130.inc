    <div class="tip">You can use format options combined with the <code>\g</code>
      metacommand to re-execute the previous query with formatting options.
      <pre><code class="hljs bash">laetitia=# select * from test;
 id | value 
----+-------
  1 | bla
  2 | bla
  3 | bla
  4 | bla
  5 | bla
  6 | bla
(6 rows)

laetitia=# \g (footer=off format=csv)
id,value
1,bla
2,bla
3,bla
4,bla
5,bla
6,bla</code></pre>This feature is available since at least Postgres 7.1.
	</div>
