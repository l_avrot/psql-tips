    <div class="tip">You can use the <code>\g | command</code> metacommand to send the
      query result to a shell command.
      <pre><code class="hljs bash">laetitia=# select setting
laetitia-# from pg_settings 
laetitia-# where name= 'data_directory';
        setting        
-----------------------
 /usr/local/pgsql/data
(1 row)

laetitia=# \g |grep 'data'
/usr/local/pgsql/data</code></pre>This feature is available since at least Postgres 7.1.
	</div>
